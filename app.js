// webrtc grab elements
let stopWebRtc = document.querySelector(".stop-video");
let startWebRtc = document.querySelector(".start-video");
let videoGrid = document.querySelector(".video-grid");
let screenshotButton = document.querySelector(".shot");
let screenshotContainer = document.querySelector(".screen-shoot-container");
let filterSelect = document.querySelector('#filter');
filterSelect.disabled = true
//video settings
const constraints = (window.constraints = {
  audio: false,
  video: true,
});
//addEventListener
startWebRtc.addEventListener("click", startFunction);
stopWebRtc.addEventListener("click", stopFunction);
// create video container
function createVideoCont() {
  const videoContainer = document.createElement("video");
  videoContainer.setAttribute("autoplay", "true");
  videoGrid.appendChild(videoContainer);
  videoContainer.style.width = "%100"
}
///stream hendle function
function handleSuccess(stream) {
  createVideoCont();
  const video = document.querySelector("video");
  const videoTracks = stream.getVideoTracks();
  console.log("Got stream with constraints:", constraints);
  console.log(`Using video device: ${videoTracks[0].label}`);
  window.stream = stream; // make variable available to browser console
  video.srcObject = stream;
  ///screenshots to canvas part
  screenshotButton.onclick = video.onclick = function () {
    const canvas = document.createElement("canvas");
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    canvas.getContext("2d").drawImage(video, 0, 0);
    /// filter class add down here
    canvas.className = filterSelect.value;
    screenshotContainer.appendChild(canvas);
  };
}
///start function async then fallow await
async function startFunction(e) {
    screenshotButton.disabled = false
    filterSelect.disabled = false
  try {
    /// start stream mye using getusermedia function()
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    handleSuccess(stream);
    e.target.disabled = true;
  } catch (e) {
    console.log(e);
  }
}
/// stop function my using webrtc track stop function
function stopFunction(e) {
  window.stream.getTracks().forEach(function (track) {
    track.stop();
  });
  const video = document.querySelector("video");
  videoGrid.removeChild(video);
  startWebRtc.disabled = false;
  filterSelect.disabled = true;
  screenshotButton.disabled = true
}

// video filter add

filterSelect.onchange = function() {
  const video = document.querySelector("video");
  video.className = filterSelect.value;
};